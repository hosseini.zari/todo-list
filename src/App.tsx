import React from 'react';
import { Container, Typography, AppBar, Toolbar } from '@mui/material';
import ToDoList from './components/ToDoList';

const App: React.FC = () => {
  return (
    <Container>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6">
            Todo List
          </Typography>
        </Toolbar>
      </AppBar>
      <ToDoList />
    </Container>
  );
}

export default App;
