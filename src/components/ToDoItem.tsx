import React from 'react';
import { ListItem, ListItemText, IconButton, ListItemSecondaryAction } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';

interface ToDoItemProps {
    todo: { id: number, title: string, isDone: boolean };
    removeTodo: (id: number) => void;
    toggleDone: (id: number) => void;
}

const ToDoItem: React.FC<ToDoItemProps> = ({ todo, removeTodo, toggleDone }) => {
    return (
        <ListItem button onClick={() => toggleDone(todo.id)} style={{ textDecoration: todo.isDone ? 'line-through' : 'none' }}>
            <ListItemText primary={todo.title} />
            <ListItemSecondaryAction>
                <IconButton edge="end" aria-label="delete" onClick={() => removeTodo(todo.id)}>
                    <DeleteIcon />
                </IconButton>
            </ListItemSecondaryAction>
        </ListItem>
    );
}

export default ToDoItem;
