import React, { useState } from 'react';
import { List, Container } from '@mui/material';
import ToDoItem from './ToDoItem';
import AddToDo from './AddToDo';

interface Todo {
    id: number;
    title: string;
    isDone: boolean;
}

const ToDoList: React.FC = () => {
    const [todos, setTodos] = useState<Todo[]>([]);

    const addTodo = (title: string) => {
        setTodos([...todos, { id: Date.now(), title, isDone: false }]);
    }

    const addDoneTask = (title: string) => {
        setTodos([...todos, { id: Date.now(), title, isDone: true }]);
    }

    const removeTodo = (id: number) => {
        setTodos(todos.filter(todo => todo.id !== id));
    }

    const toggleDone = (id: number) => {
        setTodos(todos.map(todo => todo.id === id ? { ...todo, isDone: !todo.isDone } : todo));
    }

    return (
        <Container>
            <AddToDo addTodo={addTodo} addDoneTask={addDoneTask} />
            <List>
                {todos.map(todo => (
                    <ToDoItem key={todo.id} todo={todo} removeTodo={removeTodo} toggleDone={toggleDone} />
                ))}
            </List>
        </Container>
    );
}

export default ToDoList;
