import React, { useState } from 'react';
import { TextField, Button, Box, Grid } from '@mui/material';

interface AddToDoProps {
    addTodo: (title: string) => void;
    addDoneTask: (title: string) => void;
}

const AddToDo: React.FC<AddToDoProps> = ({ addTodo, addDoneTask }) => {
    const [todoTitle, setTodoTitle] = useState('');
    const [doneTaskTitle, setDoneTaskTitle] = useState('');

    const handleAddTodo = (e: React.FormEvent) => {
        e.preventDefault();
        if (todoTitle.trim()) {
            addTodo(todoTitle);
            setTodoTitle('');
        }
    };

    const handleAddDoneTask = (e: React.FormEvent) => {
        e.preventDefault();
        if (doneTaskTitle.trim()) {
            addDoneTask(doneTaskTitle);
            setDoneTaskTitle('');
        }
    };

    return (
        <Box mt={2}>
            <Grid container spacing={3} justifyContent="center">
                <Grid item>
                    <form onSubmit={handleAddTodo}>
                        <TextField
                            label="Add Todo"
                            variant="outlined"
                            value={todoTitle}
                            onChange={(e) => setTodoTitle(e.target.value)}
                        />
                        <Button type="submit" color="primary" variant="contained" style={{ marginLeft: '10px' }}>
                            Add Todo
                        </Button>
                    </form>
                </Grid>

            </Grid>
        </Box>
    );
}

export default AddToDo;
